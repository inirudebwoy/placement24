"""
We receive jobs from third party providers/customers via xml feeds and
import the jobs into our database. The data we receive is sometimes
incomplete and we have to make up for that as good as possible. One detail
often missing is the language of the job, therefore we want you to create a
program that detects the language of a job in an xml feed.

I have attached three xml files and want you to write a python program that
can parse each xml file and print the url of the job and the language the
job is written in to stdout. You can use whatever you want. The only
requirement is that the program is written in python.

"""
import os

from lxml import etree

from guess_language import guessLanguageName


SCHEME_MAP = {'feed_a.xml': {'job_node': 'job',
                             'url_node': 'url',
                             'lang_func': ('description', )},
              'feed_b.xml': {'job_node': 'PositionOpening',
                             'url_node': 'InternetReference',
                             'lang_func': ('PositionProfile', 'lang')},
              'feed_c.xml': {'job_node': 'stellenangebot',
                             'url_node': 'url',
                             'lang_func': ('role', )}}


def get_url(element, node_name):
    """
    Retrieve text from node.

    @arg: element: root element
    @arg: node_name (str): node name to be found
    """
    return element.find('.//%s' % node_name).text


def get_lang(element, node):
    """
    Retrieve language from node or guess language based
    on node content.

    @arg: element (Element): root element
    @arg: node (tuple): (node name, attribute name), if one element it is node name
                        and it will be chekced for content

    @returns (str): language
    """
    try:
        el_name, attr_name = node
        return element.find(el_name).attrib[attr_name]
    except ValueError:  # it is not in attribute, check node itself
        return guessLanguageName(etree.tostring(element.find(node[0])))


def file_iterator(where='.'):
    """
    Iterate over xml files in folder. Yields each found.

    @arg: where (str): path to directory

    @returns (str): filename
    """
    for xml_file in os.listdir(where):
        if os.path.isfile(xml_file) and os.path.splitext(xml_file)[1] == '.xml':
            yield xml_file


def parse_xml(xml):
    """
    Worker function. It analyses each file and uses config map
    to call respective functions in order to retrieve necessary
    data.
    As it parses invalid XML by passing recover=True to iterparse
    it has guard mechanism for incorrectly closed or open nodes.

    @arg: xml (str): name of file to be processed

    """
    context = etree.iterparse(xml, events=('start', 'end'), recover=True)
    config = SCHEME_MAP.get(xml)
    _open_tag = 0
    for action, element in context:
        if element.tag == config.get('job_node') and action == 'start':
            _open_tag = 1
        if element.tag == config.get('job_node') and action == 'end':
            _open_tag = _open_tag - 1
            if not _open_tag:
                print 'url: %s ' % get_url(element, config.get('url_node'))
                print 'lang: %s' % get_lang(element, config.get('lang_func'))
                print '-' * 79
                _open_tag = 0


for f in file_iterator():
    parse_xml(f)
